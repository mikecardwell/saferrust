# SaferRust

## License

GPL-3.0. See the LICENSE file at the root of this project.

## Summary

Isolates the execution of "cargo" for rust, inside short-lived podman containers.

## Experimental

This is not a mature project. It's something I hacked together which I find
useful. I intend to make it better, but in the process expect to make backwards
incompatible changes. No doubt I've made decisions that aren't flexible enough
and need to be modified. You have been warned.

## Requirements

1. Podman
2. Python 3
3. A healthy distrust of third party libs

## Installation

1. cd /usr/local/src/
2. sudo git clone https://gitlab.com/grepular/saferrust
3. cd saferrust
4. sudo ln -s $(realpath saferrust.py) /usr/local/bin/cargo
5. Visit https://www.grepular.com/ and generously donate some money using one of
   of the links in the footer, Paypal/Bitcoin etc. After all, I probably just saved
   your ass.
6. Bookmark my blog, and subscribe to my RSS feed. You'd be crazy not to.

## Tell me more

You install my python script in your PATH as "cargo". When that script is run, it
launches a podman container from an image containing the rust compiler and cargo, and
proxies the rest of your args through. Assuming your username is "mike" and your current
working directory is /home/mike/rustproj, running the command "cargo run", will actually
launch something similar to the following command:

```
podman run -i --rm \
    --workdir /home/mike/rustproj \
    --entrypoint cargo \
    --net none \
    --mount type=bind,source=/tmp/tmp_cu29mjz,destination=/home/mike/rustproj \
    --mount type=bind,readonly,source=/home/mike/rustproj/Cargo.toml,destination=/home/mike/rustproj/Cargo.toml \
    --mount type=bind,readonly,source=/home/mike/rustproj/src,destination=/home/mike/rustproj/src \
    --mount type=bind,source=/home/mike/rustproj/target,destination=/home/mike/rustproj/target \
    -e HOME=/home/mike \
    -e USER=mike \
       docker.io/library/rust:latest \
       run
```

There is a lot to unpack there. The things you're probably wondering about:

--net none

    Network access is disabled by default. You can enable it in your .saferrust. I'll get
    to that later.

--mount

    There are several things mounted into the container, some of which are mounted readonly.
    For various reasons, I sometimes mount temporary directories, which get cleaned
    up after running the container.

-e

    Some environment variables are proxied through: HOME and USER. If you want to pass
    other environment variables in, prefix them with "SAFERRUST_". That prefix will be
    stripped before passing them through.

docker.io/library/rust:latest

    This is the podman image. It's the latest official dockerhub rust image. You can use
    a different image if you want. Again, you'll need to do that in .saferrust and I'll explain
    later.

## .saferrust config

Drop a ".saferrust" yaml file at the root of your project if you need to tweak how
things run (you will need to). This script also looks for a global config file at
$XDG_CONFIG_HOME/saferrust.yml if "XDG_CONFIG_HOME" is set, falling back to
~/.config/saferrust.yml. The global file will be read first, and anything in the
.saferrust file will override it.

The following things can be tweaked:

### mount

This is only used when running a cargo run command. It's an array of objects
containing a "path", and an optional "rw" boolean if it should be mounted rw. The
files/dirs are mounted relative to the project root. Example:

```yaml
mount:
- path: some-readonly-dir
- path: some-readwrite-dir
  rw: True
```

### network and ports

If your project needs to be able to access the network:

```yaml
network: True
```

Or to enable in host mode:

```yaml
network: host
```

If your project listens on some ports, you'll need to list them in your .saferrust in order
to expose them to the host. E.g in order to forward port 3000:

```yaml
ports:
- 3000
```

This maps to the podman arg `-p 127.0.0.1:3000:3000`

If you need to do something clevererer, you can use the full podman format. E.g this
is equivalent:

```yaml
ports:
- 127.0.0.1:3000:3000
```

A side of effect of configuring ports is that "network" is set to "True". So you don't
need to specify both.

### verbose

If you want the podman command being run, to be printed to stderr:

```yaml
verbose: True
```

### rust_version

If you want to use the "rust:1.37.0" podman image, instead of "rust:latest":

```yaml
rust_version: 1.37.0
```

### podman

If you want to use a completely different podman image named "foobar/wibble":

```yaml
podman:
    image: foobar/wibble
```

If you want to build that podman image locally, drop a Dockerfile in your
project somwhere, and refer to it:

```yaml
podman:
    image: foobar/wibble
    build: Dockerfile
```

I found this useful when I wanted to use the nightly toolchain for a particular
project. I used a Dockerfile containing the following:

```
FROM docker.io/library/rust
RUN rustup default nightly
```

## Currently supported commands

### cargo init

@TODO: Write some docs

### cargo check

@TODO: Write some docs

### cargo install

@TODO: Write some docs

### cargo run

@TODO: Write some docs

### cargo build

@TODO: Write some docs

### cargo doc

@TODO: Write some docs