#!/usr/bin/env python3

import json
import os
import re
import shutil
import subprocess
import sys
import tempfile
import yaml

homedir = os.path.expanduser('~')

podman_args = [
    '--workdir',    os.getcwd(),
    '--entrypoint', 'cargo',
]

cmd_args = []
cleanup  = []
config   = {}

def main():

    # Get the args for the cargo command to run within podman
    cmd_args.extend(sys.argv[1:])

    run_cargo()
    do_cleanup()

def run_cargo():
    parse_config()

    # The cargo command to run
    cmd = non_hyphenated_arg(cmd_args, 0)

    if cmd in [None]:
        run_podman()
    elif cmd in ['init']:
        run_cargo_init()
    elif cmd in ['run', 'check', 'build']:
        run_cargo_run()
    elif cmd in ['install']:
        run_cargo_install()
    elif cmd in ['doc']:
        run_cargo_doc()
    else:
        fail('Unsupported cargo command: {}'.format(cmd))

def set_podman_networking():

    ports = list(
        map(lambda p: p if ':' in str(p) else '127.0.0.1:{0}:{0}'.format(p),
            config.get('ports', [])
        )
    )

    for port in ports:
        podman_args.extend(['-p', port])

    if 'network' in config and config['network'] == 'host':
        podman_args.extend(['--net','host'])
    elif len(ports) == 0 and 'network' not in config:
        podman_args.extend(['--net','none'])

def run_cargo_init():

    # Mount the current working directory read/write so cargo can create the necessary files in it
    mount(os.getcwd(), rw=True)

    run_podman()

    # Add cache dir to .gitignore
    with open('.gitignore', 'a') as fh:
        fh.write('/.saferrust-cache\n')

def run_cargo_doc():
    if '--open' in cmd_args:
        open = True
        cmd_args.remove('--open')
    run_cargo_run()
    if open:
        os.system('open target/doc/' + find_project_name() + '/index.html')

def run_cargo_install():
    set_workdir('/')

    # Set --root, otherwise it installs to /usr/local/cargo/bin which is where
    # various other binaries are, so we wont know which to copy out
    root = '{}/.cargo'.format(homedir) # Default
    index = 0
    while index < len(cmd_args):
        if cmd_args[index] == '--root':
            if index < len(cmd_args) - 1:
                root = cmd_args[index+1]
                del cmd_args[index:index+2]
            else:
                root = None
            break
        index += 1
    if root != None:
        cmd_args.extend(['--root', root])

    bindir = '{}/bin'.format(root)
    tmpdir = mount_tmp(bindir)
    run_podman()
    move_recursively(tmpdir, bindir)

def run_cargo_run():
    project_root_host = require_project_root()
    project_root_tmp  = mount_tmp_project_root()

    # Configure networking
    set_podman_networking()

    # Things to mount
    mounts = config.get('mount', [])

    # Extract only paths
    mount_paths = map(lambda m: m.get('path'), mounts)
    if None in mount_paths:
        fail('Missing "path" attribute in config file mount declaration')

    # Default read-only mounts
    for path in ['Cargo.toml', 'src']:
        if not path in mount_paths: mount(os.path.join(project_root_host, path))

    # Recover the Cargo.lock file
    copy_and_restore('Cargo.lock', project_root_host, project_root_tmp)

    # Target
    if not 'target' in mount_paths:
        mount(os.path.join(project_root_host, 'target'), rw=True, create_dir=True)

    # Cache Dir. Do *not* share between projects
    cache_dir = os.path.join(project_root_host, '.saferrust-cache')
    mount(cache_dir, rw=True, create_dir=True)
    podman_args.extend(['-e', 'CARGO_HOME={}'.format(cache_dir) ])

    # User configured mounts
    for mp in mounts:

        path = os.path.join(project_root_host, mp.get('path'))
        rw = mp.get('rw', False)
        mount(path, rw=rw)

        # If the user specified a rw mount, and the path doesn't already exist, then it *may* get
        # created in the root rw tmpdir. In that case we want to persist that new file/dir, so we
        # recover it from the tmpdir before cleaning up
        if rw and not os.path.exists(path):
            newFile = os.path.join(project_root_tmp, mp.get('path'))
            cleanup.append(lambda: shutil.move(newFile, path) if os.path.exists(newFile) and not os.path.exists(path) else None)

    run_podman()

def copy_and_restore(name, host_dir, tmp_dir):
    host_path = os.path.join(host_dir, name)
    tmp_path  = os.path.join(tmp_dir, name)
    if os.path.exists(host_path):
        shutil.copyfile(host_path, tmp_path)
    cleanup.append(lambda: shutil.move(tmp_path, host_path) if os.path.exists(tmp_path) else None)

def run_podman():
    proxy_env_to_podman()

    podman_image = get_podman_image()
    podman_cmd = 'podman run -i --rm'.split(' ')
    args = [
        *podman_cmd,
        *podman_args,
        podman_image,
        *cmd_args
    ]

    if config.get('verbose', False):
        print_args = [
            #'sudo',
            *map(lambda arg: red_text(arg), podman_cmd),
            *map(lambda arg: red_text(arg), podman_args),
            '\\\n      ',
            yellow_text(podman_image),
            '\\\n      ',
            *map(lambda arg: green_text(arg), cmd_args),
        ]
        print('$ {}'.format(' '.join(print_args)), file=sys.stderr)

    subprocess.run(args)

def proxy_env_to_podman():
    for k, v in os.environ.items():
        if k in ['HOME', 'USER']:
            podman_args.extend([ '-e', '{}={}'.format(k, v) ])
        elif k.startswith('SAFERRUST_'):
            podman_args.extend([ '-e', '{}={}'.format(k[10:], v) ])

def find_project_name():
    return os.getcwd().split('/')[-1]

def find_project_root():
    dirname=os.getcwd()
    if os.path.exists(os.path.join(dirname, 'Cargo.toml')):
        return dirname

def require_project_root():
    project_root = find_project_root()
    if project_root is None:
        fail("Unable to find project root. Missing Cargo.toml in CWD")
    return project_root

def mount_tmp_project_root():
    return mount_tmp(require_project_root())

def mount_tmp(dst):
    src = tempfile.mkdtemp()
    mount(src, dst=dst, rw=True)
    cleanup.append(lambda: shutil.rmtree(src))
    return src

def set_workdir(dir):
    index = 0
    while index < len(podman_args):
        if podman_args[index] == '--workdir':
            del podman_args[index:index+2]  # Remove current item and the next item
            break  # Exit the loop after removing the items
        index += 1
    podman_args.extend(['--workdir', dir])

def parse_config():
    src = {}

    global_config_path = '{}/.config/saferrust.yml'.format(homedir)
    if 'XDG_CONFIG_HOME' in os.environ:
        global_config_path = '{}/saferrust.yml'
    if os.path.exists(global_config_path):
        with open(global_config_path) as fh:
            if 'full_load' in dir(yaml):
                src = yaml.full_load(fh)
            else:
                src = yaml.load(fh)
        for k, v in src.items():
            config[k] = v

    project_root = find_project_root()
    config_path  = None if project_root is None else os.path.join(project_root, '.saferrust')

    if config_path is not None and os.path.exists(config_path):
        with open(config_path) as fh:
            if 'full_load' in dir(yaml):
                src = yaml.full_load(fh)
            else:
                src = yaml.load(fh)

    for k, v in src.items():
        config[k] = v

def non_hyphenated_arg(args, pos=0):
    args = list(filter(lambda a: not a.startswith('-'), args))
    if len(args) > pos:
        return args[pos]

def mount(src, dst=None, rw=False, create_dir=False):

    if dst == None:
        dst = src

    if not os.path.exists(src):
        if create_dir:
            os.makedirs(src)
        else:
            return

    mount = filter(lambda o: o is not None, [
        'type=bind',
        'readonly' if not rw else None,
        'source={}'.format(src),
        'destination={}'.format(dst)
    ])
    podman_args.extend([ '--mount', ','.join(mount) ])

def move_recursively(root_src_dir, root_dst_dir):
    for src_dir, dirs, files in os.walk(root_src_dir):
        dst_dir = src_dir.replace(root_src_dir, root_dst_dir, 1)
        if not os.path.exists(dst_dir):
            os.makedirs(dst_dir)
        for file_ in files:
            src_file = os.path.join(src_dir, file_)
            dst_file = os.path.join(dst_dir, file_)
            if os.path.exists(dst_file):
                # in case of the src and dst are the same file
                if os.path.samefile(src_file, dst_file):
                    continue
                os.remove(dst_file)
            shutil.move(src_file, dst_dir)

def get_podman_image():

    rust_version      = config.get('rust_version', 'latest')
    podman_image_name = config.get('podman', {}).get('image', 'docker.io/library/rust:{}'.format(rust_version))
    podman_image_file = config.get('podman', {}).get('build')

    # If an image file was supplied, then it's a local image. Check it exists and build if not
    if podman_image_file != None:
        
        # Check if image exists
        if not exists_podman_image(podman_image_name):
            if not podman_image_file.startswith('/'):
                project_root = find_project_root()
                podman_image_file = os.path.join(project_root, podman_image_file)

            build_podman_image(podman_image_name, podman_image_file)

    return podman_image_name

def exists_podman_image(name):
    podman_cmd = ['podman', 'images', '-qf', 'reference={}'.format(name)]
    output = subprocess.check_output(podman_cmd)
    return output != b''

def build_podman_image(name, file):
    print('* Building podman image: {} from {}'.format(name, file), file=sys.stderr)
    with open(file) as fh:
        podman_cmd = ['podman', 'build', '-qt', name, '.', '-f-']
        p = subprocess.Popen(podman_cmd, stdin=subprocess.PIPE, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
        stderr = p.communicate(input=str.encode(fh.read()))[1]
        if stderr != None:
            fail(stderr)
        print('* Podman image built', file=sys.stderr)

def do_cleanup():
    while len(cleanup) > 0:
        cleanup.pop()()

def fail(err):
    print(err, file=sys.stderr)
    do_cleanup()
    sys.exit(1)

def coloured_text(colour, s):
    return '\033[{}m{}\033[0m'.format(colour, s)

def red_text(s):
    return coloured_text(91, s)

def green_text(s):
    return coloured_text(92, s)

def yellow_text(s):
    return coloured_text(33, s)

main()
